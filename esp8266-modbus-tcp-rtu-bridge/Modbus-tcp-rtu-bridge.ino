/*
  ModbusTCP to ModbusRTU bridge for ESP8266/ESP32

  Needed Libs:
    https://github.com/emelianov/modbus-esp8266
    https://github.com/emelianov/StreamBuf
    https://github.com/tzapu/WiFiManager

  You can use a ESP8266-01 and connect a MAX485 converter to it
  and may use a AMS1117 5V to 3.3V step down modul.
  Pin connection:
  ESP8266-01    MAX485     Power supply
  VCC & CH_PD   VCC        3.3V
  GND           GND        GND
  TX            DI
  RX            RO
  GRIO2         DE & RE
*/


#include <ESP8266WiFi.h>
#include <ModbusTCP.h>
#include <ModbusRTU.h>
#include <StreamBuf.h>
#include <WiFiManager.h>

#define BSIZE 1024
uint8_t buf1[BSIZE];
uint8_t buf2[BSIZE];
StreamBuf S1(buf1, BSIZE);
StreamBuf S2(buf2, BSIZE);
DuplexBuf P1(&S1, &S2);
DuplexBuf P2(&S2, &S1);

int DE_RE = 2;

const int wifiCheckInterval = 30; // intervall to check for wifi connection
unsigned long lastWifiCheck = 0;

ModbusRTU rtu;
ModbusTCP tcp;
IPAddress srcIp;

uint16_t transRunning = 0;  // Currently executed ModbusTCP transaction
uint8_t slaveRunning = 0;   // Current request slave

const int minRequestInterval = 60; // minimal intervall to poll client
unsigned long lastRequest = 0;
uint8_t lastSlave = 0;

bool cbTcpTrans(Modbus::ResultCode event, uint16_t transactionId, void* data) { // Modbus Transaction callback
  if (event != Modbus::EX_SUCCESS)                  // If transaction got an error
    Serial.printf("Modbus TCP result: %02X, Mem: %d\n", event, ESP.getFreeHeap());  // Display Modbus error code (222527)
  if (event == Modbus::EX_TIMEOUT) {    // If Transaction timeout took place
    Serial.println("Timeout");
    tcp.disconnect(tcp.eventSource());          // Close connection
    transRunning = 0;
    slaveRunning = 0;
  }
  return true;
}

bool cbRtuTrans(Modbus::ResultCode event, uint16_t transactionId, void* data) {
  if (event != Modbus::EX_SUCCESS)                  // If transaction got an error
    Serial.printf("\nModbus RTU result: %02X, Mem: %d\n", event, ESP.getFreeHeap());  // Display Modbus error code (222527)
  if (event == Modbus::EX_TIMEOUT) {    // If Transaction timeout took place
    Serial.println("Timeout");
    transRunning = 0;
    slaveRunning = 0;
  }
  return true;
}

// Callback receives raw data 
Modbus::ResultCode cbTcpRaw(uint8_t* data, uint8_t len, void* custom) {
  auto src = (Modbus::frame_arg_t*) custom;

  if (transRunning) { // Note that we can't process new requests from TCP-side while waiting for responce from RTU-side.
    Serial.print("TCP IP in - ");
    Serial.print(IPAddress(src->ipaddr));
    Serial.printf(" Fn: %02X, len: %d \n", data[0], len);
    Serial.println("Trans running");
    tcp.setTransactionId(src->transactionId); // Set transaction id as per incoming request
    tcp.errorResponce(IPAddress(src->ipaddr), (Modbus::FunctionCode)data[0], Modbus::EX_SLAVE_DEVICE_BUSY);
    return Modbus::EX_SLAVE_DEVICE_BUSY;
  }

  rtu.rawRequest(src->unitId, data, len, cbRtuTrans);
  lastRequest = millis();
  lastSlave = src->unitId;

  if (!src->unitId) { // If broadcast request (no responce from slave is expected)
    tcp.setTransactionId(src->transactionId); // Set transaction id as per incoming request
    tcp.errorResponce(IPAddress(src->ipaddr), (Modbus::FunctionCode)data[0], Modbus::EX_ACKNOWLEDGE);

    transRunning = 0;
    slaveRunning = 0;
    return Modbus::EX_ACKNOWLEDGE;
  }
  
  srcIp = IPAddress(src->ipaddr);
  slaveRunning = src->unitId;
  transRunning = src->transactionId;
  return Modbus::EX_SUCCESS;  
}

// Callback receives raw data from ModbusTCP and sends it on behalf of slave (slaveRunning) to master
Modbus::ResultCode cbRtuRaw(uint8_t* data, uint8_t len, void* custom) {
  auto src = (Modbus::frame_arg_t*) custom;
  if (!transRunning) // Unexpected incoming data
    return Modbus::EX_PASSTHROUGH;
  tcp.setTransactionId(transRunning); // Set transaction id as per incoming request
  uint16_t succeed = tcp.rawResponce(srcIp, data, len, slaveRunning);
  if (!succeed){
    Serial.println("TCP IP out - failed");
    Serial.printf("RTU Slave: %d, Fn: %02X, len: %d, ", src->slaveId, data[0], len);
    Serial.print("Response TCP IP: ");
    Serial.println(srcIp);
  }
  
  transRunning = 0;
  slaveRunning = 0;
  return Modbus::EX_PASSTHROUGH;
}

void setup() {
  Serial.begin(9600);
 
  WiFiManager wm;
  wm.autoConnect("Modbus TCP-RTU Bridge");
    
  tcp.server(); // Initialize ModbusTCP to pracess as server
  tcp.onRaw(cbTcpRaw); // Assign raw data processing callback
  
  rtu.begin(&Serial, DE_RE);  // Specify RE_DE control pin
  rtu.master(); // Initialize ModbusRTU as master
  rtu.onRaw(cbRtuRaw); // Assign raw data processing callback
}

void loop() { 
  rtu.task();
  tcp.task();

  if (lastSlave && (millis() - lastRequest > minRequestInterval * 1000)) {
    rtu.rawRequest(lastSlave, (uint8_t*) "\x01\x00\x00\x00\x01", 5, cbRtuTrans);
    lastRequest = millis();
  }

  // if WiFi is down, try reconnecting
  if ((WiFi.status() != WL_CONNECTED) && (millis() - lastWifiCheck >= wifiCheckInterval * 1000)) {
    Serial.println("WiFi reconnect ...");
    WiFi.reconnect();
    lastWifiCheck = millis();
  }

  yield();
}