# ioBroker
Mit Hilfe des ioBroker Modbus Adapter lässt sich die Therma V per Modbus abfragen und steuern. Dabei ist die richtige Konfiguration wichtig. Vor allem ist es wichtig, dass das richtige Timing gewählt wird, da die Therma V sehr empfindlich auf zu schnelles oder zu langsames Pollen reagiert. Andernfalls anwortet die Therma V nach einer weile immer häufiger mit Fehlermeldungen.

## Verbindungsparameter
Die Verbindungsparameter unterscheiden sich je nach verwendeter Hardware.
Als Geräte ID muss die Nummer eingetragen werden, die in der Therma V über das Bedienteil unter "Konnektivität > Modbus Adresse" eingestellt wurde.

### USB-Stick:
<img src="Verbindungsparameter%20USB.png" width="250">

Unter Port ist das Device des USB Sticks einzutragen. Dieser ist meistens /dev/ttyUSB0

### LAN / WLAN:
<img src="Verbindungsparameter%20LAN.png" width="250">

Unter Partner IP-Adresse muss die IP Adresse oder die URL des WLAN Gerätes eingetragen werden.

## Allgemein
Dies sind die Allgemeinen Einstellungen:

<img src="Allgemein.png" width="320">

Die Werte für den Lese- und Schreibintervall müssen je nach verwendeter Hardware angepasst werden:
- USB: 100ms
- WLAN: 30ms

## Registergruppen
Für die Konfiguration der Registergruppen können diese Dateien verwendet werden:
- [Diskrete Ausgänge](Diskrete%20Ausg%C3%A4nge)
- [Diskrete Eingänge](Diskrete%20Eing%C3%A4nge)
- [Eingangsregister](Eingangsregister)
- [Holding-Register](Holding-Register)
